"""
version 0.4.2_64w
for Celestica 
supporting bist reboot cmd  
"""
import os
import serial
import time
import sys
import string
import datetime
from utils import Logger

class STB(object):
   #####Constants 
   EXIT_SUCCESS                     = 0
   EXIT_FAIL                        = 1
   BIST_CMD_SUCCESS                 = 0
   BIST_CMD_FAIL                    = 3
   CMD_FORWARD_KEY_ERROR            = 4
   CMD_WAIT_FOR_KEY_PRESS_ERROR     = 5
   KEY_PRESS_EVENT                  = 6
   NO_KEY_PRESS_EVENT               = 7
   CMD_CONTROL_CAP_BUILD_ERROR      = 8
   BIST_CMD_TIMEOUT                 = 9
   DEVELOPMENT_BUILD                = 20
   PRODUCTION_BUILD                 = 21
   USB                              = 23
   WRONG_SW_VERSION                 = 24
   NO_RID_ERROR                     = 25
   WRITE_TO_FILE_ERROR              = 26
   BIST_VERSION_ERROR               = 29
   BIST_HANDSHAKE_SUCCESS           = 30
   BIST_HANDSHAKE_FAIL              = 31
   
   #####Attributes
   NDS_SN                           = ""
   RID                              = ""
   SN                               = ""
   test_date                        = ""
   test_time                        = ""
   key_code                         = ""
   key_press                        = ""
   status                           = ""
   bist_version                     = ""
   com_port                         = ""
   model                            = ""
   
   key_name ={
               "0A":"Select",
               "0B":"Up",
               "0C":"Down",
               "0D":"Left",
               "0E":"Right",
               "0F":"Back",
               "10":"Exit",
               "11":"Standby",
               "12":"Menu",
               "14":"Guide",
               "15":"Record",
               "1B":"Resolution"
               }
   
   def __init__(self,port_name):
      """ Intialize the STB object
      Args:
         port_name      : com port
      Returns:
      """
      self.com_port     = port_name
      self.ser = serial.Serial(
         port=port_name,
         baudrate=9600,
         timeout = 1,
         parity=serial.PARITY_NONE,
         stopbits=serial.STOPBITS_ONE,
         bytesize=serial.EIGHTBITS,
         )
   
   def close_serial(self):
      """  Close the serial port
      Args:
         
      Returns:
      """
      if self.ser.isOpen() != 0:
         self.ser.close()
      
   def open_serial(self):
      """  Open the serial port
      Args:
         
      Returns:
      """
      if self.ser.isOpen() == 0:
         self.ser.open()
   
   def acciitohex(self,str):
      """ Convert string into HEX for BIST
      Args:
         str: a string
      Returns:
         HEX: string
      """
      return ''.join(x.encode('hex') for x in str)
   
   def set_bist_version(self):
      print "Bist Version"
      self.ser.flushInput()
      self.ser.write("EA188200")
      response = self.wait_BIST_response("8820")
      [status,rec_data] = response.split(":",1)
      if int(status) != 0:
         print "Get Bist Version Failed"
         return self.BIST_VERSION_ERROR 
      print "Get Bist version Pass"
      self.bist_version = int(rec_data[12:14],16)
      return self.EXIT_SUCCESS
   
   def set_test_time(self):
      """ Get test time
      Args:
      Returns:
      """
      self.test_date = ""
      self.test_time = ""
      self.test_date = time.strftime("%x")#.replace('/','')
      self.test_time = time.strftime("%X")#.replace(':','')
   
  
   def bist_handshake(self):
      """ doing bist handshake
      Args:
      Returns:
      """
      print "PLease power on the DUT "
      print "It may take about 45 seconds to establish the connection with the unit"
      start_time =time.time()
      loop = 1
      while loop == 1 :
         end_time = time.time()
         input = ''
         input = self.ser.readline()
         if 'F0' in input: 
            # We are performing a BIST handshake             
            # send password
            self.ser.write("DTVS3bist")
            # eat the 'BIST'
            self.ser.read(4)
            while self.ser.inWaiting() > 0:
               self.ser.read(1)
            print "BIST Handshake Complete"
            loop = 0
            return self.BIST_HANDSHAKE_SUCCESS
         elif end_time - start_time > 180 :
            print "BIST Handshake time out"
            loop = 0
            return self.BIST_HANDSHAKE_FAIL
               
   def wait_BIST_response(self,command):
      """ wait for bist responses
      Args         
         command: command waited for
      Returns:
         BIST_SUCCESS   :data
         BIST_FAIL      :data
      """
      time.sleep(1)
      timeout = 10
      while timeout > 0:
         input = self.ser.readline()
         input = input.rstrip("\n")
         if input[3:7] == command:
            if input[1:2] == "2":
               print "command:",command,"succeeded"
               return str(self.BIST_CMD_SUCCESS) + ":" +input
            elif input[1:2] == "4":
               print "command:",command,"failed"
               return str(self.BIST_CMD_FAIL) + ":" + input
            elif input[1:2] == "0":
               #got ACK ignore
               print "BIST ACK:" + input
               #return BIST_ACK 
         timeout = timeout - 1
      print "unknown command timeout "+ command   
      return str(self.BIST_CMD_TIMEOUT) + ":None" 
   
   def wait_keypress(self,wait_time):
      """ 
      
      """
      loop = 1
      CMD_WAIT_FOR_KEY_PRESS = "3831"
      if wait_time <= 255:
         wait_time = hex(wait_time)
         wait_time = "00"+wait_time[2:4]
         print" the wait time : "+ wait_time
      else:
         wait_time = hex(wait_time)
         wait_time = wait_time[2:7]
         print" the wait time : "+ wait_time
      self.ser.flushInput()
      print "CMD_WAIT_FOR_KEY_PRESS: EA0" + CMD_WAIT_FOR_KEY_PRESS + "102" + wait_time + "00"
      print"Wait for button pressed in 2 minutes"
      start_time = time.time()
      self.ser.write("EA0" + CMD_WAIT_FOR_KEY_PRESS + "102" + wait_time + "00")
      while loop ==1:
         input = self.ser.readline()
         input = input.rstrip("\n")
         if input[1:2] == "9":
            print"Got the key press " + input
            self.key_code = input
            self.key_press = self.key_name[input[4:6]]
            print"key name: " + self.key_press
            return self.KEY_PRESS_EVENT
         elif input[1:2] == "4":
            print" No key pressed"
            self.key_code = "None"
            self.key_press = "None"
            print"key name: " + self.key_press
            return self.NO_KEY_PRESS_EVENT
         elif time.time() - start_time > 130:
            loop = 0
            return self.BIST_CMD_TIMEOUT
   
   def cmd_line(self,command):
      self.ser.flushInput()
      print"send command: " + command
      self.ser.write(command)
      temp = list(command)
      temp_cmd = ""
      print"comand " + temp_cmd.join(temp[3:7])
      response = self.wait_BIST_response(temp_cmd.join(temp[3:7]))
      [status,rec_data] = response.split(":",1)
      if int(status) == self.BIST_CMD_TIMEOUT:
         print"%s timeout" % (command)
         return self.BIST_CMD_TIMEOUT
      elif int(status) == self.BIST_CMD_FAIL:
         print"%s Failed" % (command)
         return self.BIST_CMD_FAIL
      elif int(status) == 0:
         print"%s pass" % (command)
         return self.BIST_CMD_SUCCESS
   
        
   def get_NDS_Serial_number(self):
      self.NDS_SN = ""
      i = 0
      CMD_SerializationGet = "7420"
      self.ser.flushInput()
      print"send: CMD_SerializationGet"
      self.ser.write("EA0" + CMD_SerializationGet + "1010900")
      response = self.wait_BIST_response(CMD_SerializationGet)
      [status,rec_data] = response.split(":",1)
      if int(status) == self.BIST_CMD_TIMEOUT:
         print"CMD_SerializationGet timeout"
         return self.BIST_CMD_TIMEOUT
      elif int(status) == self.BIST_CMD_FAIL:
         print"CMD_SerializationGet Failed"
         return self.BIST_CMD_FAIL
      elif int(status) == 0:
         print"CMD_SerializationGet pass"
         temp_NDS = rec_data[10:44]
         temp_list = list(temp_NDS)
         while i < len(temp_NDS):
            self.NDS_SN = self.NDS_SN + temp_list[i + 1]
            i += 2
         return self.BIST_CMD_SUCCESS
      
   
   def set_threshold_values(self,threshold_value,queue_value):
      """
      """
      CMD_KeySetFPCapSenseConfiguration = "3813"
      if int(threshold_value) < 15: 
         threshold = "0" + hex(threshold_value)[2:4]
      else:
         threshold = hex(threshold_value)[2:4]
      queue = "0" + hex(queue_value)[2:4]
      self.ser.flushInput()
      #print"send: CMD_KeySetFPCapSenseConfiguration : " + "EA03813102" + threshold + queue + "00"
      self.ser.write("EA03813102" + threshold + queue + "00")
      response = self.wait_BIST_response(CMD_KeySetFPCapSenseConfiguration)
      [status,rec_data] = response.split(":",1)
      if int(status) == self.BIST_CMD_TIMEOUT:
         print"CMD_KeySetFPCapSenseConfiguration timeout"
         return self.BIST_CMD_TIMEOUT
      elif int(status) == self.BIST_CMD_FAIL:
         print"CMD_KeySetFPCapSenseConfiguration Failed"
         return self.BIST_CMD_FAIL
      elif int(status) == 0:
         print"CMD_KeySetFPCapSenseConfiguration pass"
         return self.BIST_CMD_SUCCESS
      
   def forward_key_to_DP(self):
      """
      """
      CMD_FORWARD_KEY = "3811"
      self.ser.flushInput()
      print"send: CMD_FORWARD_KEY"
      self.ser.write("EA038111010100")
      response = self.wait_BIST_response(CMD_FORWARD_KEY)
      [status,rec_data] = response.split(":",1)
      if int(status) == self.BIST_CMD_TIMEOUT:
         print"CMD_FORWARD_KEY timeout"
         return self.BIST_CMD_TIMEOUT
      elif int(status) == self.BIST_CMD_FAIL:
         print"CMD_FORWARD_KEY Failed"
         return self.BIST_CMD_FAIL
      elif int(status) == 0:
         print"CMD_FORWARD_KEY pass"
         return self.BIST_CMD_SUCCESS
         
   def check_SW_version(self):
      #ret_val = BIST_CMD_FAIL
      CMD_CONTROL_CAP_BUILD = "1800"
      self.ser.flushInput()
      print"send: CMD_CONTROL_CAP_BUILD"
      self.ser.write("EA0180000")
      response = self.wait_BIST_response(CMD_CONTROL_CAP_BUILD)
      [status,rec_data] = response.split(":",1)
      if int(status) == self.BIST_CMD_TIMEOUT:
         print"CMD_CONTROL_CAP_BUILD timeout"
         return self.BIST_CMD_TIMEOUT
      elif int(status) == self.BIST_CMD_FAIL:
         print"CMD_CONTROL_CAP_BUILD Failed"
         return self.BIST_CMD_FAIL
      elif int(status) == 0:
         print"CMD_CONTROL_CAP_BUILD pass"
         if rec_data[12:16] == "0001":
            print "DEVELOPMENT_BUILD from USB"
            return self.BIST_CMD_SUCCESS
         else:
            print "NO DEVELOPMENT_BUILD NOR USB"
            return self.WRONG_SW_VERSION
   
   def set_SN(self,SN):
      """ Set SN_scanned attribute
      Args: SN
      Returns: 
      """
      self.SN = ""
      self.SN = SN
      
   def get_DUT_RID(self):
      """ get the RID from HR44
      Args:
      
      Returns:
         Pass: BIST_SUCCESS
         Fail: NO_RID_ERROR
      """
      CMD_GET_RID = "8825"
      i = 0
      self.RID = ""
      self.ser.write("EA0"+CMD_GET_RID+"0")
      response = self.wait_BIST_response(CMD_GET_RID)
      [status,rec_data] = response.split(":",1)
      if int(status) == 0:
         temp_RID = rec_data[10:34]
         temp_list = list(temp_RID)
         # get 12 digit from 24 returned digit
         while i < len(temp_RID) :
            self.RID = self.RID + temp_list[i+1]
            i += 2
         #print " in get RID " + self.RID
         return self.EXIT_SUCCESS
      elif int(status) == self.BIST_CMD_TIMEOUT:
         self.RID = "BIST_CMD_TIMEOUT"
         return self.NO_RID_ERROR         
      elif int(status) == self.BIST_CMD_FAIL:
         print"BIST_CMD_FAIL"
         return self.BIST_CMD_FAIL

   def write_log_to_file(self,str,file_name):
      """ write log to file
      Args:
         str         : string input
         file_name   : filename
      Returns:
         Pass        :EXIT_SUCCESS
         Fail        : WRITE_TO_FILE_ERROR
      """
      try:
         path = os.path.join("log",self.folder_name)
         f=open(os.path.join(self.folder_name,file_name),"wb")
         f.write(str)
         f.close()
      except :
         return self.WRITE_TO_FILE_ERROR
      return self.EXIT_SUCCESS
   
   def reboot(self):
      """reboot the STB
      Args:
      
      Returns:
      
      """
      CMD_BOXREBOOT = "0830"
      self.ser.flushInput()
      print"send: CMD_BOXREBOOT"
      self.ser.write("EA0"+CMD_BOXREBOOT+"00")
      response = self.wait_BIST_response(CMD_BOXREBOOT)
      [status,rec_data] = response.split(":",1)
      if int(status) == self.BIST_CMD_TIMEOUT:
         print"CMD_BOXREBOOT timeout"
         return self.BIST_CMD_TIMEOUT
      elif int(status) == self.BIST_CMD_FAIL:
         print"CMD_BOXREBOOT Failed"
         return self.BIST_CMD_FAIL
      elif int(status) == 0:
         print"CMD_BOXREBOOT pass"
         return self.BIST_CMD_SUCCESS
      
   def PRINT(self, string):
      print self.com_port +": " + string 
   
def test_NDS():
   com_port = raw_input("Enter Com port:")
   ##### redirect console output to file and terminal
   #sys.stdout = Logger(com)
   DUT = STB(com_port)
   DUT.open_serial()
   #DUT.bist_handshake()
   DUT.get_NDS_Serial_number()
   print" NDS number " + DUT.NDS_SN

def test_key_press_from_file():
   com_port=raw_input("Enter Com port:")
   ##### redirect console output to file and terminal
   #sys.stdout = Logger(com)
   loop = 1
   DUT = STB(com_port)
   DUT.open_serial()
   DUT.bist_handshake()
   DUT.forward_key_to_DP()
   data_file = "threshold_data.txt"
   log_name = com_port + com_port + ".txt"
   logfile = open(log_name,'w')
   logfile.write("DATE|TIME|THRESHOLD|QUEUE|KEYCODE|KEYNAME" + "\n")
   logfile.flush()
   with open(data_file) as openfileobject:
      for line in openfileobject:
         print "line===========" + line
         data = line.split(':')
         threshold = int(data[0])
         queue = int(data[1])
         print "threshold: %s queue: %s" % (threshold,queue)
         start_time = time.time()
         DUT.set_threshold_values(threshold,queue)
         while 1:
            input = DUT.ser.readline()
            input = input.rstrip("\n")
            if input[1:2] == "9":
               print "response: "+ input 
               DUT.key_code = input
               DUT.key_press = DUT.key_name[input[4:6]]
               DUT.test_date = time.strftime("%x").replace('/','')
               DUT.test_time = time.strftime("%X").replace(':','')
               logfile.write(DUT.test_date + "|" + DUT.test_time + "|" + str(threshold) + "|" + str(queue) + "|"+  DUT.key_code + "|" + DUT.key_press +"\n")
               logfile.flush()
            if time.time() - start_time > 3600:
               break         
   logfile.close()
#   except Exception, Argument:
 #     print "exception %s\n " % ( str(Argument))

def test_key_code():
   com_port=raw_input("Enter Com port:")
   ##### redirect console output to file and terminal
   #sys.stdout = Logger(com)
   loop = 1
   DUT = STB(com_port)
   DUT.open_serial()
   DUT.bist_handshake()
   DUT.forward_key_to_DP()
   DUT.set_threshold_values(12)
   
   while loop == 1:
      start_time = datetime.datetime.now()
      print"start time " + str(start_time)
      response = DUT.wait_keypress(120)      
      if response == DUT.NO_KEY_PRESS_EVENT:
         print"DUT pass"
      elif response == DUT.KEY_PRESS_EVENT:
         print" DUT failed"      
   

   
   
def cmd_line():
   loop = 1
   com_port=raw_input("Enter Com port:")
   DUT = STB(com_port)
   DUT.open_serial()
   #DUT.bist_handshake()
   while loop == 1:
      command= raw_input("enter: ")
      if command == "exit":
         loop = 0
      else:
         DUT.cmd_line(command)
   
if __name__ == "__main__":
   test_key_press_from_file()
   #cmd_line()