# bb_setup.py
from bbfreeze import Freezer
 
includes = []
excludes = ['include_py']
 
bbFreeze_Class = Freezer(distdir='dist', includes=includes, excludes=excludes)
 
#bbFreeze_Class.addScript("send_mails.py")#, gui_only=True)
bbFreeze_Class.addScript("keypress_time_v0.4.5_64w_sc.py")
bbFreeze_Class.use_compression = 0
bbFreeze_Class.include_py = True
bbFreeze_Class()
