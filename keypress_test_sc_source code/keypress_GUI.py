"""
update for bist 7.77
no checking for SW 
set the threshold value and queue to 12:1
"""

import serial
import time
import sys
import tkMessageBox
import os
import shutil
from ctypes  import *
from Tkinter import *
from threading import Thread, Lock
from DUT_Handling import STB
from utils import Logger
import datetime


mutex = Lock()
def availablePorts():
   for i in range(256):
     try:
       s = serial.Serial(i)
       s.close()
       yield 'COM' + str(i + 1)
     except serial.SerialException:
       pass

def getRIDInput(RIDEntry,Result,STB_Model):
   """ get MAC address from scanner in MACEntry
      Mac must be 12 digits 
   Args:
      MACEntry    : the MAC entry text box
      Result      : The result Label
   Returns: 
      scanned MAC address in upper case 
      or blank if failed
   """
   if STB_Model == "HR44":
      RIDInput = RIDEntry.get()
      if len(RIDInput) == 12:
         RIDEntry.configure(state = "disabled")
         return RIDInput.upper()
      elif len(RIDInput) > 0:
         Result.configure(text = "Test Not Started", bg = "gray")
      return ""
   elif STB_Model == "SHR44":
      RIDInput = RIDEntry.get()
      if len(RIDInput) == 17:
         RIDEntry.configure(state = "disable")
         return RIDInput.upper()
      elif len(RIDInput) > 0:
         Result.configure(text = "Test Not Started", bg = "gray")
      return ""
      
def getSNInput(SN_entry, RID_entry,Result, STB_Model):
   """ Get SN from scanner in MACEntry
      SN must be 14 digits 
   Args:
      SNEntry : The SN text box
      Result  : The result Label
   Returns: 
      Scanned SN in upper case 
      or blank if failed
   """
   SNInput = SN_entry.get()
   if len(SNInput) > 0:
      if (len(SNInput) == 14):
         SNInput = SNInput.upper()
         if SNInput[:2] == "G3":
            
            RID_entry.configure(state = "normal")
            Result.configure(text = "Test Not Started", bg = "white")
            return SNInput
         elif SNInput[:2] == "MV":
            
            RID_entry.configure(state = "normal")
            Result.configure(text = "Test Not Started", bg = "white")
            return SNInput
      else:   
         Result.configure(text="Wrong SN", bg="yellow")
         return "" 
   return ""
def connectToDUT(com_port, Run_btn, ComPort_lst, TestResult_lbl, DUTSN_entry, DUTRID_entry,DUTRIDRead_entry, STB_Model):   
   TestThread = Thread( target = doTest, args = (com_port, Run_btn, ComPort_lst, TestResult_lbl, DUTSN_entry, DUTRID_entry, DUTRIDRead_entry, STB_Model))     
   TestThread.setDaemon(True)
   TestThread.start()
   #button.configure(state="disabled")
   #comlist["menu"].delete(0, END)   

def doTest(com_port, Run_btn, ComPort_lst, TestResult_lbl, DUTSN_entry, DUTRID_entry, DUTRIDRead_entry, STB_Model):
   global logfile
   global mutex
   
   ##### create a new object 
   if com_port == "COM DUT":
      TestResult_lbl.configure(text="Please choose COM DUT", bg = 'yellow') 
   else:
      DUT = STB(com_port)
      DUT.model = STB_Model
      TestResult_lbl.configure(text="NO BIST Connection", bg = 'gray') 
   
      while 1:
         DUTRID_entry.configure(state = "normal")
         DUTRID_entry.delete(0,END)
         DUTSN_entry.configure(state = 'normal')
         DUTSN_entry.delete(0,END)
         DUTSN_entry.focus_set()
          
         while 1:
            if (getSNInput(DUTSN_entry,DUTRID_entry, TestResult_lbl, STB_Model) == "") :
               time.sleep(0.5)
            else:
               ##### move focus to next entry 
               #DUTRID_entry.configure(state="normal")
               #DUTRID_entry.delete(0,END)
               #DUTRID_entry.focus_set()
               if (getRIDInput(DUTRID_entry, TestResult_lbl, STB_Model) == ""):
                  time.sleep(0.5)
               else:
                  break
         
         DUTRID_entry.configure(state = 'disable')
         DUTSN_entry.configure(state = 'disable')
         try:
            ##### redirect console output to file and terminal
            #sys.stdout = Logger(com_port)
         
            DUTRIDRead_entry.configure(state = "normal")
            DUTRIDRead_entry.delete(0,END)
            DUTRIDRead_entry.configure(state = "disable")
              
            DUT.open_serial()
            DUTSN_entry.configure(state ="disable")
            DUTRID_entry.configure(state ="disable")
            TestResult_lbl.configure(text="Test Starts\nPower On DUT\n Timeout in 3 mins", bg = 'gray')
            
            current_date=time.strftime("%x").replace('/','')
            
            logfilename="log_"+current_date+".txt"
            if os.path.isfile(logfilename):
               logfile = open(logfilename, 'a')
            else:
               logfile = open(logfilename, 'a')
               logfile.write("Model|COM PORT|DATE|TIME|SN|RID|KEYCODE|BUTTON|P/F\n")
               logfile.flush()
            logfile.close() 
               
            DUT.set_test_time()
            
            #####start hand shake
            DUT.bist_handshake()
            
            start_time = time.time()
            end_time = time.time()
            TestResult_lbl.configure(text="Handshake Completed", bg = 'purple')      
            
            #####get RID number
            DUT.set_bist_version()
            if DUT.bist_version <= 76:
               print" need new bist version"
               TestResult_lbl.configure(text="Wrong version\n Need BIST 7.76 on USB\n or \nSUB Stick is not working", bg = 'yellow')
            else:
               if STB_Model == "HR44":
                  DUT.get_DUT_RID()
               elif STB_Model == "SHR44":
                  DUT.get_NDS_Serial_number()
               
                 
               DUTRIDRead_entry.configure(state = "normal")
               DUTRIDRead_entry.delete(0,END)
               DUTRIDRead_entry.insert(0,DUT.RID)
               DUTRIDRead_entry.configure(state = "disable")
                              
               if DUT.RID != DUTRID_entry.get():
                  TestResult_lbl.configure(text="   RID or NDS Mismatch   ", bg = 'yellow')
                  DUTSN_entry.configure(state ="normal")
                  DUTRID_entry.configure(state ="normal")
               
               else: 
                  DUT.SN = DUTSN_entry.get()
                  response = DUT.set_threshold_values(12,1)

                  #check_SW_version pass
                  if response == 0:      
                     DUT.forward_key_to_DP()
                     TestResult_lbl.configure(text="   Wait 2 minutes    ",bg='purple')
                     response = DUT.wait_keypress(120)      
                     #check for key press        
                     
                     if response == DUT.NO_KEY_PRESS_EVENT:
                        print"DUT pass"
                        
                        TestResult_lbl.configure(text="   DUT pass    ",bg='green')
                        DUT.status = "PASS"
                        mutex.acquire()
                        logfile = open(logfilename, 'a')  
                        logfile.write(DUT.model + "|" + DUT.com_port + "|" + DUT.test_date + "|" + DUT.test_time + "|" + DUT.SN + "|" + DUT.RID + "|" + DUT.key_code + "|" + DUT.key_press +"|" + DUT.status + "\n")
                        logfile.flush()
                        logfile.close()
                        mutex.release()
                     elif response == DUT.KEY_PRESS_EVENT:
                        print" DUT failed"
                        TestResult_lbl.configure(text="   DUT fail    ",bg='red')
                        DUT.status = "FAIL"
                        mutex.acquire()
                        logfile = open(logfilename, 'a')  
                        logfile.write(DUT.model + "|" + DUT.com_port + "|" +DUT.test_date + "|" + DUT.test_time + "|" + DUT.SN + "|" + DUT.RID + "|" + DUT.key_code + "|" + DUT.key_press + "|" + DUT.status + "\n")
                        logfile.flush()
                        logfile.close()
                        mutex.release()               
                     elif int(time.time() - start_time) > 21:
                        #loop = 0
                        TestResult_lbl.configure(text="Command Timeout\n Check connection",bg='red')
                        DUT.close_serial()
                        print" timeout "
                        #return DUT.BIST_CMD_TIMEOUT
                     
                  elif response == DUT.WRONG_SW_VERSION:
                     print "This is not a right Development build"
                     TestResult_lbl.configure(text="Not A Right SW\n Need SW Engineering 0x744",bg='red')
                  
                  elif response == DUT.BIST_CMD_TIMEOUT:
                     print"The BIST_CMD_TIMEOUT"
                     TestResult_lbl.configure(text="   Command Timeout    ",bg='red')
               
            ##### set the RID sn SN entries back to normal
            DUTSN_entry.configure(state ="normal")
            DUTRID_entry.configure(state ="normal")
            DUTRID_entry.delete(0,END)
            DUTSN_entry.delete(0,END)
               
         except serial.SerialException:
            print "The " + com_port + " is busy"


def doReset(com_port, Run_btn, ComPort_lst, TestResult_lbl):
   TestThread = Thread( target = Reset, args = (com_port,Run_btn,ComPort_lst,TestResult_lbl))     
   TestThread.setDaemon(True)
   TestThread.start()


def Reset(com_port,Run_btn,ComPort_lst,TestResult_lbl):
   DUT =STB(com_port)
   DUT.close_serial()
   
   
def BuildWindow(root, name, Amount, STB_Model):
   """ Build a new Window
      create new test window and number of DUT test GUI
   Args:
      root          : new window
      name          : name of new window
      Amount        : amount of DUTs
      STB_Model      : STB model hr44 or C41
      
   """
   root.title(name)
   mainframe = Frame(root, border=3)
   topframe  = Frame(mainframe, border=5, bd=10)
   bottomframe  = Frame(mainframe,  border=5, bd=10)
  
   for i in range(0,Amount):
     Testwindow = Frame(topframe)  
     Testwindow1 = Frame(Testwindow,background = 'BLACK', borderwidth = 1)   
     createTestWindow(Testwindow1, STB_Model)   
     Testwindow1.pack()
     Testwindow.pack(side=LEFT, ipadx=7)
   topframe.pack(fill=X, expand=1)
   bottomframe.pack(fill=X)
   mainframe.pack(fill=BOTH, expand = 1)

def ReadConnectOptions(root, amount1, Model):
   """ Read the how station and how many DUTs per station 
      destroy the welcome screen
   Args:
      root      : the root GUI
      amount1   : number of DUT on station 1
      amount2   : number of DUT on station 2
      Model     : the STB model will be tested : HR44 or C41
   Returns:
    
   """
   global AmountDUT1
   AmountDUT1=int(amount1)
   global STB_Model
   STB_Model = Model
   root.destroy()
   
def createWelcomeScreen(TestFrame, root):
   #global STB_Model   
   AmountDUTFrame = Frame(TestFrame)
   AmountDUTLB = Label(AmountDUTFrame, text="Amount of DUTs Table 1:")
   AmountDUTLB.pack(side=LEFT, padx=5)
   AmountDUTEntry = Entry(AmountDUTFrame, state="normal")
   AmountDUTEntry.pack(side=RIGHT, padx=5)
   AmountDUTFrame.pack(anchor=W)
   AmountDUTEntry.focus_set()
   '''
   AmountDUTFrame2 = Frame(TestFrame)
   AmountDUTLB2 = Label(AmountDUTFrame2, text="Amount of DUTs Table 2:")
   AmountDUTLB2.pack(side=LEFT, padx=5)
   AmountDUTEntry2 = Entry(AmountDUTFrame2, state="disable")
   AmountDUTEntry2.pack(side=RIGHT, padx=5)
   AmountDUTFrame2.pack(anchor=W)
   '''
     
   ConnectFrame = Frame(TestFrame)   
   ConnectFrame.pack(anchor=W)
   ConnectButton = Button(ConnectFrame, text="HR44", command = lambda: ReadConnectOptions(root, AmountDUTEntry.get(),"HR44" ))
   ConnectButton.pack(side=LEFT, padx=5)
   ConnectButton = Button(ConnectFrame, text="SHR44", command = lambda: ReadConnectOptions(root, AmountDUTEntry.get(),"SHR44" ))
   ConnectButton.pack(side=LEFT, padx=5)
   
       
def createTestWindow(TestFrame, STB_Model, available_ports):
   """ Create a test window
      if Model is HR44 will create SN and RID text boxes for scanned values
      if Model is C41 will create SN and MAC test boxes for scanned values
      call connectToDUT function
   Args:
      TestFrame   :
      STB_Model    : stb model will be tested HR44 or C41
   Returns:
      
   """

   AvailableCOMPorts = available_ports
   PortSelected=StringVar()
   Connect_frame = Frame(TestFrame)
   PortSelected.set("COM DUT")
   ComPort_lst = apply(OptionMenu, (Connect_frame, PortSelected) + tuple(["COM DUT"]) +tuple(AvailableCOMPorts))
   ComPort_lst.pack(side= RIGHT)
   Connect_frame.pack(anchor=W, fill = X)
   Run_btn = Button(Connect_frame, text="Start", command = lambda: connectToDUT(PortSelected.get(), Run_btn, ComPort_lst, TestResult_lbl,DUTSN_entry,DUTRID_entry,DUTRIDRead_entry, STB_Model))#, DUTMACRIDReadEntry, DUTMACRIDEntry, DUTSNEntry ))
   Run_btn.pack(side = LEFT)
   
   DUTSN_frame = Frame(TestFrame)
   DUTSN_lbl = Label(DUTSN_frame, text="DUT SN scanned:")
   DUTSN_lbl.pack(side=LEFT)
   DUTSN_entry = Entry(DUTSN_frame, state="disable")
   DUTSN_entry.pack(side=RIGHT, fill = X)
   DUTSN_frame.pack(anchor=W,fill = X)
   
   DUTRIDFrame = Frame(TestFrame)
   if STB_Model == "SHR44":  
      DUTRIDLB = Label(DUTRIDFrame, text="DUT NDS scanned:")
   if STB_Model == "HR44":
      DUTRIDLB = Label(DUTRIDFrame, text="DUT RID scanned:")
   DUTRIDLB.pack(side=LEFT)
   DUTRID_entry = Entry(DUTRIDFrame, state="disable")
   DUTRID_entry.pack(side=RIGHT)
   DUTRIDFrame.pack(anchor=W,fill = X)
     
   DUTRIDFrameRead = Frame(TestFrame)
   if STB_Model == "SHR44":
      DUTRIDReadLB = Label(DUTRIDFrameRead, text="DUT NDS Read:")
   if STB_Model == "HR44":
      DUTRIDReadLB = Label(DUTRIDFrameRead, text="DUT RID Read:")
   DUTRIDReadLB.pack(side=LEFT)
   DUTRIDRead_entry = Entry(DUTRIDFrameRead, state="disable")
   DUTRIDRead_entry.pack(side=RIGHT)
   DUTRIDFrameRead.pack(anchor=W,fill  =X)
   
   TestResult_frame = Frame(TestFrame)
   TestResult_lbl = Label( TestResult_frame, text="NO BIST Connection")
   TestResult_lbl.configure(height = 7 ,anchor = CENTER)
   TestResult_lbl.pack(fill=X)
   TestResult_frame.pack(anchor=W, fill=X)
   
def BuildWindow(root, name, Amount, STB_Model, available_ports):
   """ Build a new Window
      create new test window and number of DUT test GUI
   Args:
      root          : new window
      name          : name of new window
      Amount        : amount of DUTs
      STB_Model      : STB model hr44 or C41
      
   """
   root.title(name)
   mainframe = Frame(root, border=3)
   topframe  = Frame(mainframe, border=5, bd=10)
   bottomframe  = Frame(mainframe,  border=5, bd=10)
  
   for i in range(0,Amount):
     Testwindow = Frame(topframe)  
     Testwindow1 = Frame(Testwindow,background = 'BLACK', borderwidth = 1)   
     createTestWindow(Testwindow1, STB_Model, available_ports)   
     Testwindow1.pack()
     Testwindow.pack(side=LEFT, ipadx=7)

   topframe.pack(fill=X, expand=1)
   bottomframe.pack(fill=X)
   mainframe.pack(fill=BOTH, expand = 1)    


root = Tk()
root.title("Select Options")
mainframe = Frame(root, border=3)
topframe  = Frame(mainframe, border=5, bd=10)
AmountDUT1=0
AmountDUT2=0
createWelcomeScreen(topframe,root)
topframe.pack(fill=X)
mainframe.pack(fill=BOTH, expand = 1)
root.mainloop()
available_ports =  list(availablePorts())
root = Tk()
BuildWindow(root, "KeyPress_GUI_v0.3.6", AmountDUT1, STB_Model, available_ports)   
root.mainloop()

