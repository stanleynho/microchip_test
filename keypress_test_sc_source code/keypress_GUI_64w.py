"""
update for bist 7.77
no checking for SW 
set the threshold value and queue to 12:1
"""

import serial
import time
import sys
import tkMessageBox
import os
import shutil
from ctypes  import *
from Tkinter import *
from threading import Thread, Lock
from DUT_Handling import STB
from utils import Logger
import datetime


mutex = Lock()
def availablePorts():
   for i in range(256):
     try:
       s = serial.Serial(i)
       s.close()
       yield 'COM' + str(i + 1)
     except serial.SerialException:
       pass

def getRIDInput(RIDEntry,Result):
   """ get MAC address from scanner in MACEntry
      Mac must be 12 digits 
   Args:
      MACEntry    : the MAC entry text box
      Result      : The result Label
   Returns: 
      scanned MAC address in upper case 
      or blank if failed
   """
   RIDInput = RIDEntry.get()
   if len(RIDInput) == 12:
      RIDEntry.configure(state="disabled")
      return RIDInput.upper()
   elif len(RIDInput) > 0:
      Result.configure(text="Test Not Started", bg="gray")
   return ""      
   
def getSNInput(SN_entry,RID_entry,Result):
   """ Get SN from scanner in MACEntry
      SN must be 14 digits 
   Args:
      SNEntry : The SN text box
      Result  : The result Label
   Returns: 
      Scanned SN in upper case 
      or blank if failed
   """
   SNInput = SN_entry.get()
   if len(SNInput) > 0:
      #Result.configure(text="Test Not Started", bg="white")
      if (len(SNInput) == 14):
         SNInput = SNInput.upper()
         if SNInput[:2] == "G3":
            SN_entry.configure(state="disabled")
            RID_entry.configure(state="normal")
            Result.configure(text="Test Not Started", bg="white")
            return SNInput
      else:
         #RIDEntry.configure(state="normal")
         Result.configure(text="Wrong SN", bg="yellow")
         return "" 
   #elif len(SNInput) == 0:
      #Result.configure(text = "Test Not Started", bg="white")
   return ""  
   
def connectToDUT(com_port, Run_btn, ComPort_lst, TestResult_lbl, DUTSN_entry, DUTRID_entry,DUTRIDRead_entry):
   
   TestThread = Thread( target = doTest, args = (com_port, Run_btn, ComPort_lst, TestResult_lbl, DUTSN_entry, DUTRID_entry, DUTRIDRead_entry))     
   TestThread.setDaemon(True)
   TestThread.start()
   #button.configure(state="disabled")
   #comlist["menu"].delete(0, END)   

def doTest(com_port, Run_btn, ComPort_lst, TestResult_lbl, DUTSN_entry, DUTRID_entry, DUTRIDRead_entry):
   global logfile
   global mutex
   ##### create a new object 
   if com_port == "COM DUT":
      TestResult_lbl.configure(text="Please choose COM DUT", bg = 'yellow') 
      #DUTSN_entry.configure(state = "disable")
   else:
      try :
         DUT = STB(com_port)
         TestResult_lbl.configure(text="NO BIST Connection", bg = 'gray') 
      except serial.SerialException:
         print "The " + com_port + " is busy++++++++++"
         #DUTSN_entry.configure(state = "disable")
      while 1:
         DUTRID_entry.configure(state = "normal")
         DUTRID_entry.delete(0,END)
         #DUTRID_entry.configure(state="disable")
         
         DUTSN_entry.configure(state = 'normal')
         DUTSN_entry.delete(0,END)
         DUTSN_entry.focus_set()
          
         while (getSNInput(DUTSN_entry,DUTRID_entry, TestResult_lbl) == "") :
            time.sleep(0.5)
         
         ##### move focus to next entry 
         DUTRID_entry.configure(state="normal")
         DUTRID_entry.delete(0,END)
         DUTRID_entry.focus_set()
         
         while (getRIDInput(DUTRID_entry, TestResult_lbl) == ""):
            time.sleep(0.5)
         
         #DUTRID_entry.configure(state = 'disable')
         #DUTSN_entry.configure(state = 'disable')
         try:
            ##### redirect console output to file and terminal
            #sys.stdout = Logger(com_port)
         
            DUTRIDRead_entry.configure(state = "normal")
            DUTRIDRead_entry.delete(0,END)
            DUTRIDRead_entry.configure(state = "disable")
              
            DUT.open_serial()
            #DUTSN_entry.configure(state ="disable")
            #DUTRID_entry.configure(state ="disable")
            TestResult_lbl.configure(text="Power On DUT", bg = 'gray')
            
            current_date=time.strftime("%x").replace('/','')
            #current_date+="_"+time.strftime("%X").replace(':','')
            logfilename="log_"+current_date+".txt"
            if os.path.isfile(logfilename):
               logfile = open(logfilename, 'a')
            else:
               logfile = open(logfilename, 'a')
               logfile.write("Model|COM PORT|DATE|TIME|SN|RID|KEYCODE|P/F|KEYNAME\n")
               logfile.flush()
            logfile.close() 
         
            DUT.set_test_time()
            #####start handshake
            response = DUT.bist_handshake()
            if response == DUT.BIST_HANDSHAKE_FAIL:
               TestResult_lbl.configure(text="Handshake timeout", bg = 'yellow')
               DUTSN_entry.configure(state ="normal")
               DUTRID_entry.configure(state ="normal")
               DUTRID_entry.delete(0,END)
               DUTSN_entry.delete(0,END)
            else:
               start_time = time.time()
               end_time = time.time()
               TestResult_lbl.configure(text="Handshake Completed", bg = 'purple')      
               
               #####get RID number
               DUT.set_bist_version()
               if DUT.bist_version <= 76:
                  print" need new bist version"
                  TestResult_lbl.configure(text="USB Stick is not working", bg = 'yellow')
               else:
                  DUT.get_DUT_RID()
                  DUTRIDRead_entry.configure(state = "normal")
                  DUTRIDRead_entry.delete(0,END)
                  DUTRIDRead_entry.insert(0,DUT.RID)
                  DUTRIDRead_entry.configure(state = "disable")
               
                  if DUT.RID != DUTRID_entry.get():
                     TestResult_lbl.configure(text="   RID Mismatch   ", bg = 'yellow')
                     DUTSN_entry.configure(state ="normal")
                     DUTRID_entry.configure(state ="normal")
                  else: 
                     response = 0 #DUT.check_SW_version() 
                     #check_SW_version pass
                     if response == 0:      
                        DUT.forward_key_to_DP()
                        DUT.set_threshold_values(12,1)
                        TestResult_lbl.configure(text="   Wait 2 minutes    ",bg='purple')
                        response = DUT.wait_keypress(120)      
                        #check for key press        
                        DUT.SN = DUTSN_entry.get()
                        if response == DUT.NO_KEY_PRESS_EVENT:
                           print"DUT pass"
                           TestResult_lbl.configure(text="   DUT pass    ",bg='green')
                           DUT.status = "PASS"
                           mutex.acquire()
                           logfile = open(logfilename, 'a')  
                           logfile.write("HR44|" + DUT.com_port + "|" + DUT.test_date + "|" + DUT.test_time + "|" + DUT.SN + "|" + DUT.RID + "|" + DUT.key_code +  "|"  + DUT.status + "|" + DUT.key_press +"\n")
                           logfile.flush()
                           logfile.close()
                           mutex.release()
                        elif response == DUT.KEY_PRESS_EVENT:
                           print" DUT failed"
                           TestResult_lbl.configure(text="   DUT fail    ",bg='red')
                           DUT.status = "FAIL"
                           mutex.acquire()
                           logfile = open(logfilename, 'a')  
                           logfile.write("HR44|" + DUT.com_port + "|" +DUT.test_date + "|" + DUT.test_time + "|" + DUT.SN + "|" + DUT.RID + "|" + DUT.key_code + "|" + DUT.status + "|" + DUT.key_press +"\n")
                           logfile.flush()
                           logfile.close()
                           mutex.release()               
                        elif int(time.time() - start_time) > 21:
                           #loop = 0
                           TestResult_lbl.configure(text="Command Timeout",bg='yellow')
                           DUT.close_serial()
                           print" timeout "
                           #return DUT.BIST_CMD_TIMEOUT
                        
                     elif response == DUT.WRONG_SW_VERSION:
                        print "This is not a right Development build"
                        TestResult_lbl.configure(text="Need SW Engineering 0x744",bg='yellow')
                     
                     elif response == DUT.BIST_CMD_TIMEOUT:
                        print"The BIST_CMD_TIMEOUT"
                        TestResult_lbl.configure(text="   Command Timeout    ",bg='yellow')
                  
               ##### set the RID sn SN entries back to normal
               DUTSN_entry.configure(state ="normal")
               DUTRID_entry.configure(state ="normal")
               DUTRID_entry.delete(0,END)
               DUTSN_entry.delete(0,END)
                  
         except serial.SerialException:
            print "The " + com_port + " is busy"


def doReset(com_port, Run_btn, ComPort_lst, TestResult_lbl):
   TestThread = Thread( target = Reset, args = (com_port,Run_btn,ComPort_lst,TestResult_lbl))     
   TestThread.setDaemon(True)
   TestThread.start()


def Reset(com_port,Run_btn,ComPort_lst,TestResult_lbl):
   DUT =STB(com_port)
   DUT.close_serial()
   
   
def BuildWindow(root, name, Amount, STBModel):
   """ Build a new Window
      create new test window and number of DUT test GUI
   Args:
      root          : new window
      name          : name of new window
      Amount        : amount of DUTs
      STBModel      : STB model hr44 or C41
      
   """
   root.title(name)
   mainframe = Frame(root, border=1)
   topframe  = Frame(mainframe, border=1, bd=2)
   bottomframe  = Frame(mainframe,  border=1, bd=2)
  
   #for i in range(0,Amount):
   Testwindow = Frame(topframe)  
   Testwindow1 = Frame(Testwindow,background = 'BLACK', borderwidth = 1)   
   createTestWindow(Testwindow1, STBModel)   
   Testwindow1.pack()
   Testwindow.pack(side=LEFT, ipadx=1)
   
   topframe.pack(fill=X, expand=1)
   bottomframe.pack(fill=X)
   mainframe.pack(fill=BOTH, expand = 1)

def ReadConnectOptions(root, amount1, Model):
   """ Read the how station and how many DUTs per station 
      destroy the welcome screen
   Args:
      root      : the root GUI
      amount1   : number of DUT on station 1
      amount2   : number of DUT on station 2
      Model     : the STB model will be tested : HR44 or C41
   Returns:
    
   """
   '''
   global AmountDUT1
   AmountDUT1=int(amount1)
   global STBModel
   STBModel = Model
   root.destroy()
   '''
   global AmountDUT1
   AmountDUT1=int(amount1)
   global STBModel
   STBModel = Model
   root.destroy()
   
def createWelcomeScreen(TestFrame, root):
   global STBModel   
   AmountDUTFrame = Frame(TestFrame)
   AmountDUTLB = Label(AmountDUTFrame, text="Amount of DUTs Table 1:")
   AmountDUTLB.pack(side=LEFT, padx=5)
   AmountDUTEntry = Entry(AmountDUTFrame, state="normal")
   AmountDUTEntry.pack(side=RIGHT, padx=5)
   AmountDUTFrame.pack(anchor=W)
   AmountDUTEntry.focus_set()
   '''
   AmountDUTFrame2 = Frame(TestFrame)
   AmountDUTLB2 = Label(AmountDUTFrame2, text="Amount of DUTs Table 2:")
   AmountDUTLB2.pack(side=LEFT, padx=5)
   AmountDUTEntry2 = Entry(AmountDUTFrame2, state="normal")
   AmountDUTEntry2.pack(side=RIGHT, padx=5)
   AmountDUTFrame2.pack(anchor=W)
   '''  
   ConnectFrame = Frame(TestFrame)   
   ConnectFrame.pack(anchor=W)
   ConnectButton = Button(ConnectFrame, text="HR44", command = lambda: ReadConnectOptions(root, AmountDUTEntry.get(),"HR44" ))
   ConnectButton.pack(side=LEFT, padx=5)
   #ConnectButton = Button(ConnectFrame, text="C41", command = lambda: ReadConnectOptions(root, AmountDUTEntry.get(),"C41" ))
   #ConnectButton.pack(side=LEFT, padx=5)
   
       
def createTestWindow(TestFrame, STBModel, available_com_port):
   """ Create a test window
      if Model is HR44 will create SN and RID text boxes for scanned values
      if Model is C41 will create SN and MAC test boxes for scanned values
      call connectToDUT function
   Args:
      TestFrame   :
      STBModel    : stb model will be tested HR44 or C41
   Returns:
      
   """

   AvailableCOMPorts = available_com_port
   PortSelected=StringVar()
   Connect_frame = Frame(TestFrame)
   PortSelected.set("COM DUT")
   ComPort_lst = apply(OptionMenu, (Connect_frame, PortSelected) + tuple(["COM DUT"]) +tuple(AvailableCOMPorts))
   ComPort_lst.pack(side= RIGHT)
   Connect_frame.pack(anchor=W, fill = X)
   Run_btn = Button(Connect_frame, text="Start", command = lambda: connectToDUT(PortSelected.get(), Run_btn, ComPort_lst, TestResult_lbl,DUTSN_entry,DUTRID_entry,DUTRIDRead_entry))#, DUTMACRIDReadEntry, DUTMACRIDEntry, DUTSNEntry ))
   Run_btn.pack(side = LEFT)
   #Reset_btn = Button(Connect_frame, text="Reset", command = lambda: doReset(PortSelected.get(), Run_btn, ComPort_lst, TestResult_lbl))#, DUTMACRIDReadEntry, DUTMACRIDEntry, DUTSNEntry ))
   #Reset_btn.pack(side = LEFT)
   
   DUTSN_frame = Frame(TestFrame)
   DUTSN_lbl = Label(DUTSN_frame, text="SN:")
   DUTSN_lbl.pack(side=LEFT)
   DUTSN_entry = Entry(DUTSN_frame, state="disable")
   DUTSN_entry.pack(side=RIGHT, fill = X)
   DUTSN_frame.pack(anchor=W,fill = X)
   
   DUTRIDFrame = Frame(TestFrame)
   if STBModel == "C41":  
      DUTRIDLB = Label(DUTRIDFrame, text="MAC1:")
   if STBModel == "HR44":
      DUTRIDLB = Label(DUTRIDFrame, text="RID1:")
   DUTRIDLB.pack(side=LEFT)
   DUTRID_entry = Entry(DUTRIDFrame, state="disable")
   DUTRID_entry.pack(side=RIGHT)
   DUTRIDFrame.pack(anchor=W,fill = X)
     
   DUTRIDFrameRead = Frame(TestFrame)
   if STBModel == "C41":
      DUTRIDReadLB = Label(DUTRIDFrameRead, text="MAC2:")
   if STBModel == "HR44":
      DUTRIDReadLB = Label(DUTRIDFrameRead, text="RID2:")
   DUTRIDReadLB.pack(side=LEFT)
   DUTRIDRead_entry = Entry(DUTRIDFrameRead, state="disable")
   DUTRIDRead_entry.pack(side=RIGHT)
   DUTRIDFrameRead.pack(anchor=W,fill = X)
   
   TestResult_frame = Frame(TestFrame)
   TestResult_lbl = Label( TestResult_frame, text="NO BIST Connection")
   TestResult_lbl.configure(height = 2 ,anchor = CENTER)
   TestResult_lbl.pack(fill=X)
   TestResult_frame.pack(anchor=W, fill=X)
   
def BuildWindow(root, name, Amount, STBModel, available_com_port):
   """ Build a new Window
      create new test window and number of DUT test GUI
   Args:
      root          : new window
      name          : name of new window
      Amount        : amount of DUTs
      STBModel      : STB model hr44 or C41
      
   """
   root.title(name)
   mainframe = Frame(root, border=1)
   topframe  = Frame(mainframe, border=1, bd=1)
   bottomframe  = Frame(mainframe,  border=1, bd=1)
  
   for i in range(0,Amount):
     Testwindow = Frame(topframe)  
     Testwindow1 = Frame(Testwindow,background = 'BLACK', borderwidth = 1)   
     createTestWindow(Testwindow1, STBModel, available_com_port)   
     Testwindow1.pack()
     Testwindow.pack(side=LEFT, ipadx=1)

   topframe.pack(fill=X, expand=1)
   bottomframe.pack(fill=X)
   mainframe.pack(fill=BOTH, expand = 1)    

  
root = Tk()
root.title("Select Options")
mainframe = Frame(root, border=1)
topframe  = Frame(mainframe, border=1, bd=1)
AmountDUT1=0
AmountDUT2=0
createWelcomeScreen(topframe,root)
topframe.pack(fill=X)
mainframe.pack(fill=BOTH, expand = 1)
root.mainloop()

NUM_SLOT = 8
root =Tk()
newWindow = root
num_table = AmountDUT1/NUM_SLOT  + 1

available_com_port = list(availablePorts())
for i in range(1,num_table):
   BuildWindow(newWindow, "Key Press App V0.3.6_64windows",NUM_SLOT , STBModel, available_com_port)
   
if num_table % NUM_SLOT  > 0 :
   BuildWindow(newWindow, "Key Press App V0.3.6_64windows_8mins", AmountDUT1 % NUM_SLOT  , STBModel, available_com_port)
   
root.mainloop()


