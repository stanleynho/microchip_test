"""
update for bist 7.77
no checking for SW 
read threshold from file 

##### v0.4.3
testing at 
<low_threshold>:2:1:60
12:1:30
20:3:27
50:3:27
64:3:50
##### v0.4.4
run a full test
"""

import serial
import time
import sys
import traceback
import os
import shutil
import datetime
from ctypes  import *
from Tkinter import *
from threading import Thread, Lock
from DUT_Handling import STB
from utils import Logger


class GUI(object):
   def __init__(self):
      self.DUT = None
      self.mutex_logfile = Lock()
      _log_file = None
      self.result_dict = None
   
   def test_with_threshold(self, data_line, log_file_name):
      """ wait for the key press at certain threshold value, queuce value and duration 
         Args:
            data_line : string threshold:queue:duration
            
         Returns:
            string : 0: _test_date, _test_time, str(_threshold),str(_queue), _key_code, _key_press) 
      """
      self.DUT.PRINT ("test with: " + data_line)
      _ret_val = None
      _str_result = ""
      _data = data_line.split(":")
      _threshold = int(_data[0])
      _queue = int(_data[1])
      _duration = int(_data[2])
      _key_code = None
      _key_press = None
      _start_run_time = time.time()
      _status = None
      self.DUT.set_threshold_values(_threshold, _queue)
      _first_run = False
      _first_key = "None"
      while 1:
         self.DUT.ser.flushOutput()
         _input = self.DUT.ser.readline()
         _input = _input.rstrip("\n")
         if _input[1:2] == "9":
            self.DUT.PRINT( "response: "+ _input)
            _date = time.strftime("%x")
            _time = time.strftime("%X")
            _key_code = _input
            try: 
               _key_press = self.DUT.key_name[_input[4:6]]
            except Exception, e:
               #self.exception_msg(e)
               self.DUT.PRINT(_input + " is invalid key\n")
               pass
            else:
               _str_result = "%s|%s|%s\n" % ( str(_threshold), str(_queue), _key_press)  
               if _first_run == False:
                  _first_key = _key_press
                  _first_run = True
               ##### write to log
               self.mutex_logfile.acquire()
               _log_file = open(os.path.join("logs",log_file_name),"a")
               _log_file.write(self.DUT.com_port + "|" + self.DUT.RID + "|" + _date + "|" + _time + "|" + _str_result )
               _log_file.flush()
               self.mutex_logfile.release()
               
               ##### update the status
               _status = "Failed"
         if time.time() - _start_run_time > _duration:
            break 
      if  _status == "Failed":
         self.result_dict[data_line] = "Failed|" + str(_first_key)
      else :
         self.result_dict[data_line] = "Passed|" + str(_first_key)
      
      #for key, value in self.result_dict.items():
        # print "+=================="+ key + "____" + value
   
   def check_all_sensors(self, data_line, log_file_name):
      """ wait for the key press at certain threshold value, queuce value and duration 
         Args:
            data_line : string threshold:queue:duration
            
         Returns:
            string : 0: _test_date, _test_time, str(_threshold),str(_queue), _key_code, _key_press) 
      """
      self.DUT.PRINT("check all sensors")
      _key_count ={
               "Select" : 0,
               "Up" : 0,
               "Down" : 0,
               "Left" : 0 ,
               "Right" : 0,
               "Standby" : 0,
               "Menu" : 0,
               "Guide" : 0,
               "Record" : 0 ,
               "Resolution" : 0
               }
      
      _ret_val = None
      _str_result = None
      _data = data_line.split(":")
      _threshold = int(_data[0])
      _queue = int(_data[1])
      _duration = int(_data[2])
      _key_code = None
      _key_press = None
      _start_run_time = time.time()
      _status = "Passed|None"
      
      self.DUT.set_threshold_values(_threshold, _queue)
      while 1:
         self.DUT.ser.flushInput()
         _input = self.DUT.ser.readline()
         _input = _input.rstrip("\n")
         if _input[1:2] == "9": 
            _date = time.strftime("%x")
            _time = time.strftime("%X")
            _key_code = _input 
            try: 
               _key_press = self.DUT.key_name[_input[4:6]]
            except Exception, e:
               self.DUT.PRINT(_input + " is invalid key\n")
               pass
            else:
               _key_count[_key_press] = _key_count[_key_press] + 1
               _str_result = "%s|%s|%s\n" % (str(_threshold), str(_queue), _key_press)
               
               ##### write to log file 
               self.mutex_logfile.acquire()
               _log_file = open(os.path.join("logs",log_file_name),"a")
               _log_file.write(self.DUT.com_port + "|" + self.DUT.RID + "|" + _date + "|" + _time + "|" + _str_result )
               _log_file.flush()
               self.mutex_logfile.release()
            
         if time.time() - _start_run_time > _duration:
            break

      for key_name, value in _key_count.items() :
         if value == 0:
            #update the status
            _status = "Failed|" + key_name 
        

         #print str(key_name) + ":" + str(value)        
      self.result_dict[data_line] = _status
      
   def doTest(self, com_port, main_logfile, mutex_main_logfile):
      """
      """
       
      test_all_sensors_data = "3:1:60" ##### default values
      sensor_data = list()
      data_file = "threshold_data.txt"
      count = 0 
      _str_result = ""
      _temp_str = ""
      self.result_dict = dict()
      
      try:
         ##### reading the sensor setting from file
         with open(data_file) as file_object :
            for line in file_object:
               if "<low_threshold>" in line :
                  test_all_sensors_data = line.rstrip("\n").split(":",1)[1]
                  self.result_dict[test_all_sensors_data] = "0"
               elif "#" not in line and  "<low_threshold>" not in line:
                  sensor_data.append(line.rstrip("\n"))
                  self.result_dict[line.rstrip("\n")] = "0"
            
         ##### create a new object 
         if com_port == "COM DUT":
            self.TestResult_lbl.configure(text="Please choose COM DUT", bg = 'yellow') 
         else:
            self.DUT = STB(com_port)
            self.DUT.status = "Passed"
            self.TestResult_lbl.configure(text="NO BIST Connection", bg = 'gray') 
            
            ##### redirect console output to file and terminal
            sys.stdout = Logger(com_port)
            
            self.DUT.open_serial()
            self.TestResult_lbl.configure(text="Power On DUT", bg = 'gray')
            
            #####start handshake
            response = self.DUT.bist_handshake()      
            #response = 0
            if response == self.DUT.BIST_HANDSHAKE_FAIL:
               self.TestResult_lbl.configure(text="Handshake timeout", bg = 'yellow')
               self.DUT.close_serial()
            else:
               start_time = time.time()
               end_time = time.time()
               self.DUT.set_test_time()
               self.TestResult_lbl.configure(text="Handshake Completed", bg = 'purple')                     
               self.DUT.set_bist_version()
               if self.DUT.bist_version <= 76:
                  self.DUT.PRINT("need new bist version")
                  self.TestResult_lbl.configure(text="USB Stick is not working", bg = 'yellow')
               else:
                  #####start the test
                  self.TestResult_lbl.configure(text="Test Starts", bg = 'purple')                     
                  
                  #####get RID number
                  self.DUT.get_DUT_RID()
                  
                  #####create a log for each DUT
                  _log_file_name = "%s_%s.txt" % (com_port,self.DUT.RID)
                  self.mutex_logfile.acquire()
                  _log_file = open(os.path.join("logs",_log_file_name),"w+")
                  _log_file.write("COMPORT|RID|DATE|TIME|THRESHOLD|QUEUE|KEYNAME\n")
                  _log_file.flush()
                  self.mutex_logfile.release()
                  
                  ##### forward the key press to stdout
                  self.DUT.forward_key_to_DP()
                  
                  self.TestResult_lbl.configure(text = "threshold:queue:time(s)\n" + test_all_sensors_data , bg = 'purple')                     
                  self.check_all_sensors(test_all_sensors_data, _log_file_name)
                     
                  for threshold_value in sensor_data:
                     self.TestResult_lbl.configure(text = "threshold:queue:time(s)\n" + threshold_value, bg = 'purple')                     
                     self.test_with_threshold(threshold_value, _log_file_name)
                     
                  #####Done testing, write the result to main_log 
                  for key, value in self.result_dict.items():
                     if value.split("|",1)[0] == "Failed":
                        self.DUT.status = "Failed"
                        
                  if self.DUT.status == "Failed":
                     self.TestResult_lbl.configure(text = "Failed Unit", bg = 'red')
                  else: 
                     self.TestResult_lbl.configure(text = "Pass Unit", bg = 'green')
                  
                  for key, value in self.result_dict.items():
                     _temp_str =  _temp_str +"|"+ value
                  #print "-------------"+ _temp_str
                  _str_result = "%s|%s|%s|%s|%s%s\n" % (com_port, self.DUT.RID, self.DUT.test_date,
                                                               self.DUT.test_time, self.DUT.status, _temp_str)
                  mutex_main_logfile.acquire()
                  mainlogfile = open(main_logfile,"a")
                  mainlogfile.write(_str_result)
                  mainlogfile.flush()
                  mainlogfile.close()
                  mutex_main_logfile.release()
                  self.DUT.close_serial()
              
      except serial.SerialException, e:
         #self.exception_msg(e)
         self.DUT.PRINT(str(e.args))
      except IOError, e :
         self.exception_msg(e)
         self.TestResult_lbl.configure(text = "No Threshold_data.txt", bg = 'yellow')
      except Exception, e :
         self.exception_msg(e)
               
   def exception_msg(self,e):
      traceback.print_exc(file=sys.stdout)
   
   def test_window(self, TestFrame, STBModel, main_logfile, mutex_main_logfile, available_ports):
      """ 
      Create a test window
         if Model is HR44 will create SN and RID text boxes for scanned values
         if Model is C41 will create SN and MAC test boxes for scanned values
         call connectToDUT function
      Args:
         TestFrame   :
         STBModel    : stb model will be tested HR44 or C41
      Returns:

      """
      self.AvailableComPorts = available_ports 
      self.PortSelected=StringVar()
      self.Connect_frame = Frame(TestFrame)
      self.PortSelected.set("COM DUT")
      self.ComPort_lst = apply(OptionMenu, (self.Connect_frame, self.PortSelected) + tuple(["COM DUT"]) +tuple(self.AvailableComPorts))
      self.ComPort_lst.config(width=7)
      self.ComPort_lst.pack(side= RIGHT)
      self.Connect_frame.pack(anchor=W, fill = X)
      self.Run_btn = Button(self.Connect_frame, text="Start", command = lambda: self.connectToDUT(self.PortSelected.get(), main_logfile, mutex_main_logfile))
      self.Run_btn.pack(side = LEFT)
      Reset_btn = Button(self.Connect_frame, text="Reset", command = lambda: self.doReset(self.PortSelected.get(), self.Run_btn, self.ComPort_lst, self.TestResult_lbl))
      Reset_btn.pack(side = LEFT)
      self.TestResult_frame = Frame(TestFrame)
      self.TestResult_lbl = Label( self.TestResult_frame, text="NO BIST Connection")
      self.TestResult_lbl.configure(height = 2 ,anchor = CENTER)
      self.TestResult_lbl.pack(fill=X)
      self.TestResult_frame.pack(anchor=W, fill=X)
   
   def doReset(self, com_port, Run_btn, ComPort_lst, TestResult_lbl):
      TestThread = Thread( target = self.Reset, args = (com_port, Run_btn,ComPort_lst,TestResult_lbl))     
      TestThread.setDaemon(True)
      TestThread.start()
       
   def Reset(self, com_port,Run_btn,ComPort_lst,TestResult_lbl):
      try: 
         self.DUT.close_serial()
         self.TestResult_lbl.configure(text="No BIST Connection", bg = 'white')
      except Exception,e:
         self.exception_msg(e)
         pass
     
   def connectToDUT(self, com_port, main_logfile, mutex_main_logfile):
      global TestThread
      #print com_port
      TestThread = Thread( target = self.doTest, args = (com_port,  main_logfile, mutex_main_logfile))
      TestThread.setDaemon(True)
      TestThread.start()
          
class WelcomeScreen(object):
   def __init__(self, root):
      self.amount_DUT = None
      self.stb_model = None
      self.root = root
   
   def create_welcome_screen(self, frame):
      
      self.AmountDUTFrame = Frame(frame)
      self.AmountDUTLB = Label(self.AmountDUTFrame, text="Amount of DUTs Table 1:")
      self.AmountDUTLB.pack(side=LEFT, padx=5)
      self.AmountDUTEntry = Entry(self.AmountDUTFrame, state="normal")
      self.AmountDUTEntry.pack(side=RIGHT, padx=5)
      self.AmountDUTFrame.pack(anchor=W)
      self.AmountDUTEntry.focus_set()
      
      self.ConnectFrame = Frame(frame)   
      self.ConnectFrame.pack(anchor=W)
      self.ConnectButton = Button(self.ConnectFrame, text="HR44", command = lambda: self.destroy_welcome_screen(self.AmountDUTEntry.get(), "HR44" ))
      self.ConnectButton.pack(side=LEFT, padx=5)
         
   
   def destroy_welcome_screen(self,amount_DUT, stb_model): 
      self.root.destroy()
      self.amount_DUT = amount_DUT
      self.stb_model = stb_model
      return 0
    
def build_test_window( root, name, Amount, stb_model,main_logfile, mutex_main_logfile, available_ports):
      """ Build a new Window
         create new test window and number of DUT test GUI
      Args:
         root          : new window
         name          : name of new window
         Amount        : amount of DUTs
         STBModel      : STB model hr44 or C41
         
      """
      root.title(name)
      mainframe = Frame(root, border=1)
      topframe  = Frame(mainframe, border=1, bd=1)
     
      objs_GUI = [ GUI() for i in range(int(Amount))]
      for obj_GUI in objs_GUI:  
         Testwindow = Frame(topframe)  
         Testwindow1 = Frame(Testwindow,background = 'BLACK', borderwidth = 1)   
         obj_GUI.test_window( Testwindow1, stb_model, main_logfile, mutex_main_logfile, available_ports)
         Testwindow1.pack()
         Testwindow.pack(side=LEFT, ipadx=1)
      
      topframe.pack(fill=X, expand=1)
      mainframe.pack(fill=BOTH, expand = 1)     

def availablePorts():
      for i in range(256):
         try:
            s = serial.Serial(i)
            s.close()
            yield 'COM' + str(i + 1)
         except serial.SerialException:
            pass
         except Exception, e:
            self.exception_msg(e)
            
def create_folder(folder_name):
   """Create folder name for saving log
   Args:
   
   Returns:
   
   """
   try:
      if os.path.exists(folder_name):
         pass
         #shutil.rmtree(folder_name)
         #os.makedirs(folder_name)
      else:
         os.makedirs(folder_name)
   except Exception, e:
      exc_type, exc_obj, exc_tb = sys.exc_info()
      fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
      self.DUT.PRINT ("Type: %s \nfilename: %s \nline no: %s \n%s" % (exc_type, fname, exc_tb.tb_lineno, e))
      
if __name__ == '__main__':
   """
   """
   _amount_DUT = None
   _stb_model = None
   _str_title_line =""
   _title_dict = dict()
   ##### Create the root for the welcome screen
   root = Tk()
   root.title("Select Options")
   mainframe = Frame(root, border=3)
   topframe = Frame(mainframe, border=3, bd=3)
   welcome_gui = WelcomeScreen(root)
   welcome_gui.create_welcome_screen(topframe)
   topframe.pack(fill=X)
   mainframe.pack(fill=BOTH, expand = 1)
   root.mainloop()
   _amount_DUT = welcome_gui.amount_DUT 
   _stb_model = welcome_gui.stb_model
   
   ##### read the threshold data file 
   try:
      data_file = "threshold_data.txt"
      with open(data_file) as file_object :
         for line in file_object:
            if "<low_threshold>" in line :
               test_all_sensors_data = line.rstrip("\n").split(":",1)[1]
               _title_dict[test_all_sensors_data] = "0"
            elif "#" not in line and  "<low_threshold>" not in line:
               
               _title_dict[line.rstrip("\n")] = "0"
      for key in _title_dict.keys():
         _str_title_line = _str_title_line + "|" + key + "|" + key + "_keyname"
      ##### Write the data to log file   
      main_logfile = "main_log.txt"
      mainlogfile = open(main_logfile, 'w+')
      mainlogfile.write("COMPORT|RID|TEST DATE|TEST TIME|P/F" + _str_title_line +"\n" )
      mainlogfile.flush()
      mainlogfile.close()
      create_folder("logs")

      #get available com port 
      available_ports = list(availablePorts())

      ##### Create a root for Test Frame
      NUM_SLOT = 8
      root = Tk()
      newWindow = root
      num_table = int(_amount_DUT)/NUM_SLOT  + 1
      mutex_main_logfile = Lock()
      for i in range(1,num_table):
         build_test_window(newWindow, "Key Press App V0.4.4_64windows_sc",NUM_SLOT, _stb_model, main_logfile, mutex_main_logfile, available_ports)    
      if num_table % NUM_SLOT  > 0 :
         build_test_window(newWindow, "Key Press App V0.4.4_64windows_sc", int(_amount_DUT) % NUM_SLOT, _stb_model, main_logfile, mutex_main_logfile, available_ports)
      root.mainloop()
   except Exception, e:
      traceback.print_exc(file=sys.stdout)
   
  

