import os
import serial
import time
import sys
import re
import string
import shutil

class Logger(object):
   """
   redirect log to terminal and log file
   """
   def __init__(self, filename):
      self.terminal = sys.stdout
      filename=filename+filename+".txt"
      self.log = open(filename, "a")

   def write(self, message):
      self.terminal.write(message)
      self.log.write(message)